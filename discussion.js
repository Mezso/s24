db.inventory.insertMany([
{
	name : "Captain America Shield",
	price : 50000,
	qty: 17,
	company : "Hydra and Co"
},
{
	name : "Mjonirl",
	price : 75000,
	qty: 24,
	company : "Asgard Production"
},
{
	name : "Iron Man Suit",
	price : 25400,
	qty: 25,
	company : "Stark Industry"
},
{
	name : "Eye of Agamotto",
	price : 28000,
	qty: 51,
	company : "Sanctum Company"
},
{
	name : "Iron Spider Suit",
	price : 30000,
	qty: 24,
	company : "Stark Industry"
}

])

// Query Operators
	//Allows us to be more flexible when querying in MongDB, we can opt to find, update and delete documents based on some condition isntead of just specifici criterias

// Comparison Query Operators
	// $gt and $gte
	// Syntax:
		db.collection.find({field:{$gt:value}})
		db.collection.find({field:{$gt:value}})
	// $gt - greater than, allows us to find values that are greater than the given value.
	// $gte - greater than or equal , allows us to find values that are greater than or equal to the given value.

	dv.inventory.find({price:{$gte:75000}})

	// $lt and $lte
	// Syntax
		db.collection.find({field: {$lt: value}})
		db.collection.find({field: {$lte: value}})

	//  $lt - less than, allows us to find values that are less than the given value.
	//  $lte - less than or equal, allows us to find values that are less or equal the given value.

	db.inventory.find({qty:{$lt:20}})

	// $ne
	// Syntax
		db.collection.find({field:{$ne:value}})

	// $ne - not equal , returns a document that values are not equal to the given value

	db.inventory.find({qty :{$ne:10}})

	// $in
	// Syntax
		db.collection.find({field:{$in:value}})

	// $in - allow to find docoments that satisfy either of the specified values

	db.inventory.find({price:{$in:[25400,30000]}})

	// REMINDER: Although, you can express this query using $or operator, choose the $in operator rather than $or operator when performing equality checks on the same field.


	db.inventory.find({price : {$gte : 50000}})
	db.inventory.find({qty : {$in : [24,16]}})

	//  UPDATE and DELETE
		// 2 arguments namely : query criteria, update
		db.inventory.updateMany({qty:{$lte : 24}},{$set :{isActive:true}})


	db.inventory.updateMany({price:{$lt:28000}},{$unset:{qty:17}})

	// Logical Operator
		// $and
		// Syntax:
			db.collection.find({and:[{criteria1},{criteria2}]})

		// $and - allows us to return document/s that satisfies all the given conditions.

		db.inventory.find({$and : [{price:{$gte:50000}},{qty:17}]})

	//  $or
	//  Syntax
		db.collection.find({$or:[{criteria1},{criteria2}])

	// $or - allows us to return documents that satisfied either of the given conditions.

		db.inventory.find({$or:[{qty:{$lt : 24}},{isActive}]})

		db.inventory.updateMany({$or:[{qty:{$lte:24}},{price:{$lte:30000}}]},{$set :{isActive:true

	// Evaluaton Query Operator
		//$regex
		// Syntax
		{field:{regex: /pattern/}}
			// case sensitive query
		{field:{regex: /pattern/, $options: '$optionValue'}}

		db.inventory.find({name:{$regex: 'S'}})

		db.inventory.find({name:{$regex: 'A',$options:'$i'}})


		db.inventory.find({$and:[{name:{$regex:'i'}},{price:{$gt:70000}}]})


	// Field Projection
		// allows us to hide/show properties of a returned documents after a query. When dealing with complex data structures, there might be instance when fields are not useful for the query that we are trying to accomplish.

		// Inclusion and Exclusion
			//Syntax:
				db.collections.find({criteria},{field: 1})

				// field : 1 - allows us to include/add specific fields only when retrieving documents. The value provided is a1 to denote that the field is beign included.

				db.inventory.find({},{name:1})

		// Syntax :
		db.collections.find({criteria},{field:0})
		// field : 0 - allows us to exclude the specific field.
			db.inventory.find({},{qty:0})

		// REMINDER : When using field projection, field inclusion and exclusion may not be used in the same time. Excluding the "_id" field.

		db.inventory.find({company:{$regex:'A'}},{name:1,company:1,_id:0})

		db.inventory.find({$and:[{name:{$regex:'A'}},{price:{$lte:30000}}]},{name:1,price:1,_id:0})